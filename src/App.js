import './App.css';
import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Cita from './Cita';

function App() {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Cita} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
