import React, { useState, useEffect } from "react";
import './estilos.css';
import { Container, Row, Col } from 'reactstrap';

const Cita = () => {

    const api_url = 'http://cites-random-project.herokuapp.com/api/citarandom';

    const [obj, setObj] = useState({});
    useEffect(() => {
        fetch(api_url)
            .then(data => data.json())
            .then(datos => setObj(datos))
            .catch(err => console.log(err))

    }, [])

    return (
        <>
            <Container>
                <Row>
                    <Col className="order-lg-1 order-sm-2" sm={12} lg={6}>
                        <br />
                        <div className="foto">
                            <img src={obj.url} alt={obj.nombre} />
                        </div>
                    </Col>
                    <Col className="order-lg-2 order-sm-1" sm={12} lg={6}>
                        <br />
                        <div>
                            {obj.texto}
                        </div>
                        <div>
                            —{obj.nombre}
                        </div>
                        <br />
                    </Col>


                </Row>
            </Container>
        </>
    )
}

export default Cita;